using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ColorTextoBoton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {
    public GameObject textoNaranja;
    public GameObject texto;

   public void OnPointerEnter(PointerEventData eventData) {
        textoNaranja.SetActive(true);
        texto.SetActive(false);
   }

    public void OnPointerExit(PointerEventData eventData) {
        textoNaranja.SetActive(false);
        texto.SetActive(true);
    }
}
