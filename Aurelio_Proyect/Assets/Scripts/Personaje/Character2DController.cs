using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Character2DController : MonoBehaviour {
    //Variables movimiento basico
    public float MovementSpeed = 1;
    public float JumpForce = 1;
    private float JumpCount;
    private Rigidbody2D _rigidbody;
    public BoxCollider2D boxCollider2D;
    public float Caida;

    //Vida
    public int health;
    public int maxHealth;

    //Caida Animacion
    public Animator animacion;

    //Variables para Dash
    public float dashDistance = 15f;
    bool isDashing;
    float doubleTapTime;
    KeyCode lastKeyCode;
    float mx;
    public float timeDashPositivo = 0.5f;
    public float timeDashNegativo = -0.5f;
    

    //Variables de suelo
    public LayerMask ground;
    public float distanciaRay;

    //Colecionable movimiento
    public bool movimientoRapido;

    //Coleccionable vida
    public GameObject pressE;

    //Coleccionable invencibilidad
    public bool invencible;

    //Coleccionable tiempo
    public float tiempo;

    //Checkpoint
    public Vector3 posicionIncial;

    //Aparici�n pociones
    public GameObject pocionInvencibilidad;
    public GameObject pocionDesplazamiento;


    void Start() {
        _rigidbody = GetComponent<Rigidbody2D>();
        maxHealth = 2;
        health = maxHealth;
        pressE.gameObject.SetActive(false);
        posicionIncial = transform.position;
    }

    // Update is called once per frame
    void Update() {

        pressE.gameObject.SetActive(false);

        if (!isDashing) {
            //Movimiento
            var movement = Input.GetAxis("Horizontal");
            if (movement <= -0.3f || movement >= 0.3f)
            {
                mx = movement;
            } else {
                mx = 0;
            }


            //Flip personaje
            if (Input.GetAxis("Horizontal") < 0) {
                GetComponent<SpriteRenderer>().flipX = false;
            }
            if (Input.GetAxis("Horizontal") > 0) {
                GetComponent<SpriteRenderer>().flipX = true;
            }

            //Animacion Correr
            if (movement <= -0.3f || movement >= 0.3f){
                animacion.SetBool("Correr", true);
            } else {
                animacion.SetBool("Correr", false);
            }



            //Animacion Caida / Salto
            if (_rigidbody.velocity.y < 0) {
                _rigidbody.AddForce(new Vector2(0, Caida), ForceMode2D.Impulse);
                animacion.SetBool("Caer", true);
                animacion.SetBool("Saltar", false);
            } else {
                animacion.SetBool("Caer", false);
            }

            //Salto del personaje
            if (Input.GetButtonDown("Jump") && Mathf.Abs(_rigidbody.velocity.y) < 0.001f && IsGrounded()) {
                animacion.SetBool("Saltar", true);
                _rigidbody.AddForce(new Vector2(0, JumpForce), ForceMode2D.Impulse);
                JumpCount++;
            } else if (_rigidbody.velocity.y <= 0 && IsGrounded()) {
                JumpCount = 0;
            }

            //Dash izquierdo
            if (Input.GetKeyDown(KeyCode.A) && JumpCount <= 1) {
                if (doubleTapTime > Time.time && lastKeyCode == KeyCode.A) {
                    StartCoroutine(Dash(timeDashNegativo));
                    JumpCount++;
                } else {
                    doubleTapTime = Time.time + 0.3f;
                }
                lastKeyCode = KeyCode.A;

            }

            //Dash derecho
            if (Input.GetKeyDown(KeyCode.D) && JumpCount <= 1) {
                if (doubleTapTime > Time.time && lastKeyCode == KeyCode.D) {
                    StartCoroutine(Dash(timeDashPositivo));
                    JumpCount++;
                } else {
                    doubleTapTime = Time.time + 0.5f;
                }
                lastKeyCode = KeyCode.D;
            }
        }

        //Recoger Coleccionable Vida
       
        RaycastHit2D hit;
        if (GetComponent<SpriteRenderer>().flipX) {
            hit = Physics2D.Raycast(transform.position + Vector3.up + Vector3.right, Vector2.right, distanciaRay);
            //Debug.DrawRay(transform.position + Vector3.up + Vector3.right, Vector2.right, Color.red, 1);
            Debug.DrawLine(transform.position + Vector3.up + Vector3.right, transform.position + Vector3.up + Vector3.right + new Vector3(distanciaRay, 0, 0));
        } else {
            hit = Physics2D.Raycast(transform.position + Vector3.up + Vector3.left, Vector2.left, distanciaRay);
            //Debug.DrawRay(transform.position + Vector3.up + Vector3.left, Vector2.left, Color.red, 1);
            Debug.DrawLine(transform.position + Vector3.up + Vector3.left, transform.position + Vector3.up + Vector3.left + new Vector3(- distanciaRay, 0, 0));
        }
            


        //Activar elemento pop up que muestre la tecla para coger el item y coja la vida
        if (hit.collider != null && hit.collider.CompareTag("Vida")) {
            pressE.gameObject.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E)) {
                health++;
                Destroy(hit.collider.gameObject);
                pressE.gameObject.SetActive(false);
            }
            
        } else

        //Colecionable desplazamiento
        if (hit.collider != null && hit.collider.CompareTag("Movimiento")) {
            pressE.gameObject.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E)) {
                movimientoRapido = true;
                MovementSpeed = 20;
                animacion.speed += 0.5f;
                Destroy(hit.collider.gameObject);
                Invoke("MovimientoRapidoFalse", 5.0f);
                pressE.gameObject.SetActive(false);
                pocionDesplazamiento.SetActive(true);
            }
        }

        //Coleccionable invencibilidad
        if (hit.collider != null && hit.collider.CompareTag("Invencible")) {
            pressE.gameObject.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E)) {
                invencible = true;
                Destroy(hit.collider.gameObject);
                Invoke("InvencibilidadFalse", 10f);
                pressE.gameObject.SetActive(false);
                pocionInvencibilidad.SetActive(true);
            }
        }

        //Coleccionable tiempo
        if (hit.collider != null && hit.collider.CompareTag("Tiempo"))
        {
            pressE.gameObject.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E))
            {
                GameObject.FindObjectOfType<TimeController>().timeValue += tiempo;
                Destroy(hit.collider.gameObject);
                pressE.gameObject.SetActive(false);
            }

        }
    }

    // Consumible invencibilidad bool
    void InvencibilidadFalse()
    {
        invencible = false;
        pocionInvencibilidad.SetActive(false);
    }

    // Consumible movimiento bool
    void MovimientoRapidoFalse() {
        movimientoRapido = false;
        MovementSpeed = 8;
        pocionDesplazamiento.SetActive(false);
    }

    public void DIE() {
        transform.position = posicionIncial;
        animacion.SetBool("Die", false);
    }

    //Recibir da�o
    public void TakeHit() {
        if(invencible == false) {
            health--;
            animacion.SetBool("Die", true);
            if (health <= 0)
            {
                Destroy(gameObject);
            }
        }
    }


    //Dash
    void FixedUpdate() {
        if (!isDashing)
            _rigidbody.velocity = new Vector2(mx * MovementSpeed, _rigidbody.velocity.y);
    }
  
    //Dash
    IEnumerator Dash(float direction) {
        isDashing = true;
        animacion.SetBool("Dash", true);
        animacion.SetBool("Correr", false);
        _rigidbody.velocity = new Vector2(_rigidbody.velocity.x, 0f);
        _rigidbody.AddForce(new Vector2(dashDistance * direction, 0f), ForceMode2D.Impulse);
        float gravity = _rigidbody.gravityScale;
        _rigidbody.gravityScale = 0;
        yield return new WaitForSeconds(0.4f);
        isDashing = false;
        animacion.SetBool("Dash", false);
        _rigidbody.gravityScale = gravity;
    }

    //Mirar si toca el suelo
    bool IsGrounded() {
        Vector2 position = transform.position;
        Vector2 direction = Vector2.down;
        float distance = 0.8f;
        RaycastHit2D hit = Physics2D.Raycast(position, direction, distance, ground);
        Debug.DrawRay(position, direction, Color.green, 1);

        if (hit.collider != null) {
            //Debug.Log(hit.collider.name);
            return true;
        }
        return false;
        
    }
   
}
