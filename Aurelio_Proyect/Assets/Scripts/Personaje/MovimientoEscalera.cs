using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoEscalera : MonoBehaviour
{
    private float vertical;
    public float speed = 8f;
    private bool enEscalera;
    private bool enEscalada;
    public Rigidbody2D rb;
    private float _gravedad;
    public Animator animacion;

    private void Start() {
        _gravedad = rb.gravityScale;
    }
    void Update()
    {
        vertical = Input.GetAxis("Vertical"); 
        if( enEscalera && Mathf.Abs(vertical)>0f && !enEscalada) {
            enEscalada = true;
            rb.gravityScale = 0f;
            animacion.SetBool("Escaleras", true);
        }
    }
    private void FixedUpdate()
    {
        if (enEscalada) {
            rb.velocity = new Vector2(rb.velocity.x, vertical * speed);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Escalera")) {
            enEscalera = true;
        }
        
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Escalera")) {
            enEscalera = false;
            enEscalada = false;
            rb.gravityScale = _gravedad;
            animacion.SetBool("Escaleras", false);
        }

    }
}
