using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Inicio : MonoBehaviour
{

    public void Play() {
        SceneManager.LoadScene("Nivel1");
    }

    public void Exit() {
        Application.Quit();
    }
}
