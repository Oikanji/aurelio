using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TimeController : MonoBehaviour
{
    public float timeValue;
    float minutes;
    float seconds;
    public Text timeText;


    private void Start() {
        timeValue = 90;
       
    }

    private void Update() {
        if(timeValue > 0){
            timeValue -= Time.deltaTime;
        }
        else {
            timeValue = 0;
        }

        DisplayTime(timeValue);
    }

    void DisplayTime(float timeTodisplay) {
        if(timeTodisplay < 0) {
            timeTodisplay = 0;
        }
        else {
            timeTodisplay += 1;
        }
        minutes = Mathf.FloorToInt(timeTodisplay / 60);
        seconds = Mathf.FloorToInt(timeTodisplay % 60);
        timeText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
