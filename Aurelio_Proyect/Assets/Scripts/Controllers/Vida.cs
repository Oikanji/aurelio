using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vida : MonoBehaviour
{
    public Character2DController player;
    public GameObject full;
    public GameObject endFull;
    public GameObject vida3;
    public GameObject fullVida3;


    // Update is called once per frame
    void Update()
    {
        if(player.health == 3) {
            vida3.SetActive(true);
            fullVida3.SetActive(true);
        }

        if(player.health == 2) {
            fullVida3.SetActive(false);
            full.SetActive(true);
        }

        if (player.health == 1)
        {
            full.SetActive(false);
        }

        if (player.health == 0)
        {
            endFull.SetActive(false);
        }
    }
}
