using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PausaController : MonoBehaviour {

    //Pausa
    public bool pause;
    public GameObject pauseMenu;


    // Start is called before the first frame update
    void Start() {

        //Desactivar Pausa

        pauseMenu.SetActive(false);
        pause = false;
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update() {

        //Activar Pausa
        
        if (Input.GetKeyDown(KeyCode.Escape)) {
            
            if (Time.timeScale == 0) {
                //Debug.Log("se ejecuta no pausa");
                pauseMenu.SetActive(false);
                Time.timeScale = 1;
            }
            else {
                //Debug.Log("se ejecuta pausa");
                Time.timeScale = 0;
                pauseMenu.SetActive(true);
            }
        }
    }

    public void Resume() {
        pauseMenu.SetActive(false);
        Time.timeScale = 1;
    }

    public void QuitGame() {
        Application.Quit();
    }

    public void Inicio()
    {
        SceneManager.LoadScene("Inicio");
    }
}
